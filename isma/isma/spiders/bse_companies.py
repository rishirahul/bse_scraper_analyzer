# create DB and set DB name
# create table COMPANY and COMPANY_DATA as mentioned
# check if last page is 131 and change accordingly
# run using: scrapy crawl isma_companies
import scrapy
from selenium import webdriver
import time
from scrapy.http import TextResponse
import json
import MySQLdb
from selenium.common.exceptions import NoSuchElementException

class QuotesSpider(scrapy.Spider):
    name = "isma_companies"
    start_urls = [
        'http://www.bseindia.com/corporates/List_Scrips.aspx'
    ]
    companies = []
    def __init__(self):
        self.driver = webdriver.Chrome()

    def parseCompanyData(self, resp):
        rows = resp.xpath('//table[@id="ctl00_ContentPlaceHolder1_gvData"]/tbody/tr')
        rowNum = 0;
        for row in rows:
            rowNum = rowNum + 1
            if (rowNum == 1) or (rowNum == 2) or (rowNum == len(rows)):
                continue
            company = {}
            company['scode'] =  row.xpath('td[1]/a/text()').extract()[0].encode('utf-8')
            company['slink'] =  row.xpath('td[1]/a/@href').extract()[0].encode('utf-8')
            company['sname'] =  row.xpath('td[2]/text()').extract()[0].encode('utf-8')
            company['cname'] =  row.xpath('td[3]/text()').extract()[0].encode('utf-8')
            company['cstatus'] =  row.xpath('td[4]/text()').extract()[0].encode('utf-8')
            company['fval'] =  row.xpath('td[6]/text()').extract()[0].encode('utf-8')
            company['indust'] =  row.xpath('td[8]/text()').extract()[0].encode('utf-8')
            company['inst'] =  row.xpath('td[9]/text()').extract()[0].encode('utf-8')
            self.companies.append(company)


    def insertCompanyDataInTable(self, cursor, securityCode, securityLink, securityName, compName, secStatus, faceVal, industry, instrument):
        sql = """INSERT IGNORE INTO COMPANY(scode, slink, \
                sname, cname, cstatus, fval, indust, inst) \
            VALUES ('{0}', '{1}', '{2}', '{3}', '{4}' \
            , '{5}', '{6}', '{7}')""".format(securityCode, securityLink.replace('\'', '\'\''), \
            securityName.replace('\'', '\'\''), compName.replace('\'', '\'\''), secStatus.replace('\'', '\'\''),  \
            faceVal.replace('\'', '\'\''), industry.replace('\'', '\'\''), instrument.replace('\'', '\'\''))
        cursor.execute(sql)

    def printCompanyDataFromTable(self, cursor):
        # sql = "SELECT * FROM EMPLOYEE \
        #    WHERE INCOME > '%d'" % (1000)
        sql = """SELECT * FROM COMPANY WHERE scode > 0"""
        try:
            # Execute the SQL command
            cursor.execute(sql)
            # Fetch all the rows in a list of lists.
            results = cursor.fetchall()
            for row in results:
                securityCode = row[0]
                securityLink = row[1]
                securityName = row[2]
                compName = row[3]
                faceVal = row[5]
                # Now print fetched result
                print ("securityCode=%d,securityLink=%s,securityName=%s,compName=%s,faceval=%s" % \
                        (securityCode, securityLink, securityName, compName, faceVal ))
        except:
            print ("Error: unable to fecth data")

    def checkTableExists(self, dbcon, tablename):
        dbcur = dbcon.cursor()
        dbcur.execute("""
            SELECT COUNT(*)
            FROM information_schema.tables
            WHERE table_name = '{0}'
            """.format(tablename.replace('\'', '\'\'')))
        if dbcur.fetchone()[0] == 1:
            dbcur.close()
            return True
        dbcur.close()
        return False

    def createCompanyTableIfDNE(self, db, cursor):
        if (self.checkTableExists(db, "COMPANY")):
            print("table aleady found")
        else: 
            print("error: creating table")
            sql = '''CREATE TABLE COMPANY (
            scode INT NOT NULL,
            slink TEXT DEFAULT NULL,
            sname VARCHAR(250) DEFAULT NULL,
            cname VARCHAR(250) DEFAULT NULL,
            cstatus VARCHAR(250) DEFAULT NULL,
            fval VARCHAR(250) DEFAULT NULL,
            indust VARCHAR(250) DEFAULT NULL,
            inst VARCHAR(250) DEFAULT NULL,
            PRIMARY KEY ( scode )
            ) ENGINE=MyISAM DEFAULT CHARSET=latin1
            '''
            cursor.execute(sql)

    def createCompanyDataTableIfDNE(self, db, cursor):
        if (self.checkTableExists(db, "COMPANY_DATA")):
            print("table aleady found")
        else:
            print("error: creating table")
            sql = '''CREATE TABLE COMPANY_DATA ( sdate DATE NOT NULL, scode INT NOT NULL, oprice FLOAT(16,2) NOT NULL, cprice FLOAT(16,2) NOT NULL, 
            hprice FLOAT(16,2) NOT NULL, lprice FLOAT(16,2) NOT NULL, avgprice FLOAT(16,2) NOT NULL, nshares INT NOT NULL, ntrades INT NOT NULL, 
            delqty INT NOT NULL, PRIMARY KEY ( scode, sdate )) PARTITION BY HASH(scode) 
            '''
            cursor.execute(sql)

    def parseStoreCompanyData(self, resp, cursor):
        rows = resp.xpath('//table[@id="ctl00_ContentPlaceHolder1_gvData"]/tbody/tr')
        rowNum = 0;
        for row in rows:
            rowNum = rowNum + 1
            if (rowNum == 1) or (rowNum == 2) or (rowNum == len(rows)):
                continue
            scode =  row.xpath('td[1]/a/text()').extract()[0]
            #.decode('utf-8')
            slink =  row.xpath('td[1]/a/@href').extract()[0]
            #.decode('utf-8')
            sname =  row.xpath('td[2]/text()').extract()[0]
            cname =  row.xpath('td[3]/text()').extract()[0]
            cstatus =  row.xpath('td[4]/text()').extract()[0]
            fval =  row.xpath('td[6]/text()').extract()[0]
            indust =  row.xpath('td[8]/text()').extract()[0]
            inst =  row.xpath('td[9]/text()').extract()[0]
            print(scode)
            print(slink)
            self.insertCompanyDataInTable(cursor, int(scode), slink, sname, cname, cstatus, fval, indust, inst)
            
    def parse(self, response):
        self.driver.get(response.url)
        # Open database connection
        db = MySQLdb.connect("localhost","root","mayur_1")
        cursor = db.cursor()
        # createDBifDNE(db, 'demoDb')
        sql = 'use IndianStockData'
        cursor.execute(sql)
        # create company table -- worst case -- prefer manually
        # self.createCompanyTableIfDNE(db,cursor)
        # create company data table -- worst case -- prefer manually 
        # self.createCompanyDataTableIfDNE(db,cursor)

        time.sleep(3)
        # fill company search table get all equity mutual funds and companies 
        self.driver.find_element_by_xpath("//select[@name='ctl00$ContentPlaceHolder1$ddSegment']/option[text()='Equity']").click()
        self.driver.find_element_by_xpath("//select[@name='ctl00$ContentPlaceHolder1$ddlStatus']/option[text()='Active']").click()
        self.driver.find_element_by_xpath("//input[@name='ctl00$ContentPlaceHolder1$btnSubmit']").click()
        time.sleep(2)
        resp = TextResponse(url=response.url, body=self.driver.page_source, encoding='utf-8')
        # read company data and store
        self.parseStoreCompanyData(resp, cursor)
        # click next page till page 131
        for i in range(2,131):
            print(i)
            time.sleep(2)
            try:
                self.driver.find_element_by_xpath("//a[text()=%d]" % i).click() 
            except NoSuchElementException:
                time.sleep(2)
                print(i)
                self.driver.find_element_by_xpath("(//a[text()='...'])[2]").click() 
                self.driver.implicitly_wait(5)
                time.sleep(20)
            resp = TextResponse(url=response.url, body=self.driver.page_source, encoding='utf-8')
            # read company data and store
            self.parseStoreCompanyData(resp, cursor)

        # print company data reading from DB. just for verification
        self.printCompanyDataFromTable(cursor)
        time.sleep(2)
        self.driver.close()
