# create DB and set DB name
# create table COMPANY and COMPANY_DATA as mentioned
# check if last page is 131 and change accordingly
# run using: scrapy crawl isma_companies
import scrapy
from selenium import webdriver
import time
from scrapy.http import TextResponse
import json
import MySQLdb
import os
from selenium.common.exceptions import NoSuchElementException

class QuotesSpider(scrapy.Spider):
    name = "isma_dup"
    start_urls = [
    ]
    companies = []
    def __init__(self):
        db = MySQLdb.connect("localhost","root","mayur_1")
        cursor = db.cursor()
        sql = 'use IndianStockData'
        cursor.execute(sql)
        sql = """SELECT * FROM COMPANY WHERE scode > 0"""
        cursor.execute(sql)
        results = cursor.fetchall()
        for row in results:
            securityCode = row[0]
            url = 'https://www.bseindia.com/markets/equity/EQReports/StockPrcHistori.aspx?scripcode=%d&flag=sp&Submit=G' % securityCode
            self.start_urls.append(url)
        self.driver = webdriver.Chrome()


    def insertCompanyDataInTable(self, cursor, secDate, secDateUni, secCode, opprice, clprice, hiprice, loprice, avgrprice, numshares, numTrades, delivQty):
        a = "('{0}', '{1}', '{2}', '{3}', '{4}' , '{5}', '{6}', '{7}', '{8}', '{9}' )".format(secDateUni, secCode, opprice, clprice, hiprice, loprice, avgrprice, numshares, numTrades, delivQty)
        print (a);
        sql = """INSERT IGNORE INTO COMPANY_DATA (sdateuni, scode, oprice, cprice, hprice, lprice, avgprice, nshares, ntrades, delqty) VALUES ({0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}' )""".format((secDateUni), str(secCode), str(opprice), str(clprice), str(hiprice), str(loprice), str(avgrprice),  \
            str(numshares), str(numTrades), str(delivQty))
        try:
            cursor.execute(sql)
        except Exception as e:
            print ("The following query failed:")
            print (sql) 
            print (e)

    def parseStoreCompanyData(self, resp, cursor):
        seccode = resp.xpath('//span[@id="ctl00_ContentPlaceHolder1_lblScripCodeValue"]/text()').extract()[0]
        csvfile = '/media/HDD/workspace/indianStockAnalysis/csvData/%s.csv' % seccode
        print(seccode)
        rows = resp.xpath('//span[@id="ctl00_ContentPlaceHolder1_spnStkData"]/table/tbody/tr')
        rowNum = 0
        if (os.path.exists(csvfile)):
            for row in rows:
                rowNum = rowNum + 1
                if (rowNum == 1)  or (rowNum == 2) or (rowNum > 7):
                    continue
                secDate = row.xpath('td[1]/text()').extract()[0]
                struct_time = time.strptime(secDate, "%d/%m/%y")
                secDateUni = (struct_time.tm_year*10000)+(struct_time.tm_mon*100)+(struct_time.tm_mday)
                secCode = seccode
                opprice = row.xpath('td[2]/text()').extract()[0].replace(",", "")
                hiprice = row.xpath('td[3]/text()').extract()[0].replace(",", "")
                loprice = row.xpath('td[4]/text()').extract()[0].replace(",", "")
                clprice = row.xpath('td[5]/text()').extract()[0].replace(",", "")
                avgrprice = row.xpath('td[6]/text()').extract()[0].replace(",", "")
                numshares = row.xpath('td[7]/text()').extract()[0].replace(",", "")
                numTrades = row.xpath('td[8]/text()').extract()[0].replace(",", "")
                delivQty = row.xpath('td[10]/text()').extract()[0].replace(",", "")
                print(secCode)
                print(secDate)
                self.insertCompanyDataInTable(cursor, secDate, secDateUni, secCode, opprice, clprice, hiprice, loprice, avgrprice, numshares, numTrades, delivQty)
            
    def parse(self, response):
        self.driver.get(response.url)
        # Open database connection
        db = MySQLdb.connect("localhost","root","mayur_1")
        cursor = db.cursor()
        # createDBifDNE(db, 'demoDb')
        sql = 'use IndianStockData'
        cursor.execute(sql)
        # create company table -- worst case -- prefer manually
        # self.createCompanyTableIfDNE(db,cursor)
        # create company data table -- worst case -- prefer manually 
        # self.createCompanyDataTableIfDNE(db,cursor)

        time.sleep(3)
        # fill company search table get all equity mutual funds and companies 
        resp = TextResponse(url=response.url, body=self.driver.page_source, encoding='utf-8')
        # read company data and store
        self.parseStoreCompanyData(resp, cursor)
        # click next page till page 131
        time.sleep(2)
        # self.driver.close()