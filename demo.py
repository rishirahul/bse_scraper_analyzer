#!/usr/bin/python
import MySQLdb

def checkTableExists(dbcon, tablename):
    dbcur = dbcon.cursor()
    dbcur.execute("""
        SELECT COUNT(*)
        FROM information_schema.tables
        WHERE table_name = '{0}'
        """.format(tablename.replace('\'', '\'\'')))
    if dbcur.fetchone()[0] == 1:
        dbcur.close()
        return True
    dbcur.close()
    return False

def createDBifDNE(db, databaseName):
    cursor = db.cursor()
    sql = 'CREATE DATABASE IF NOT EXISTS {0}'.format(databaseName.replace('\'', '\'\''))
    cursor.execute(sql)
    cursor.close()

def insertCompanyDataInTable(cursor, securityCode, securityLink, securityName, compName, secStatus, faceVal, industry, instrument):
    sql = """INSERT IGNORE INTO COMPANY(scode, slink, \
            sname, cname, cstatus, fval, indust, inst) \
         VALUES ('{0}', '{1}', '{2}', '{3}', '{4}' \
         , '{5}', '{6}', '{7}')""".format(securityCode, securityLink.replace('\'', '\'\''), \
         securityName.replace('\'', '\'\''), compName.replace('\'', '\'\''), secStatus.replace('\'', '\'\''),  \
         faceVal.replace('\'', '\'\''), industry.replace('\'', '\'\''), instrument.replace('\'', '\'\''))
    cursor.execute(sql)

def printCompanyDataFromTable(cursor):
    # sql = "SELECT * FROM EMPLOYEE \
    #    WHERE INCOME > '%d'" % (1000)
    sql = """SELECT * FROM COMPANY WHERE scode > 0"""
    try:
        # Execute the SQL command
        cursor.execute(sql)
        # Fetch all the rows in a list of lists.
        results = cursor.fetchall()
        for row in results:
            securityCode = row[0]
            securityLink = row[1]
            securityName = row[2]
            compName = row[3]
            faceVal = row[5]
            # Now print fetched result
            print ("securityCode=%d,securityLink=%s,securityName=%s,compName=%s,faceval=%s" % \
                    (securityCode, securityLink, securityName, compName, faceVal ))
    except:
        print ("Error: unable to fecth data")

# Open database connection
db = MySQLdb.connect("localhost","root","mayur_1")
cursor = db.cursor()
createDBifDNE(db, 'demoDb')
sql = 'use demoDb'
cursor.execute(sql)
if (checkTableExists(db, "COMPANY")):
    print("table aleady found")
else: 
    print("creating table")
    sql = '''CREATE TABLE COMPANY (
	  scode INT NOT NULL,
	  slink TEXT DEFAULT NULL,
	  sname VARCHAR(250) DEFAULT NULL,
	  cname VARCHAR(250) DEFAULT NULL,
	  cstatus VARCHAR(250) DEFAULT NULL,
	  fval VARCHAR(250) DEFAULT NULL,
	  indust VARCHAR(250) DEFAULT NULL,
	  inst VARCHAR(250) DEFAULT NULL,
      PRIMARY KEY ( scode )
	  ) ENGINE=MyISAM DEFAULT CHARSET=latin1
	  '''
    cursor.execute(sql)
securityCode = 124
securityLink = "http://google.com"
securityName = "goog" 
compName = "google" 
secStatus = "active" 
faceVal = "23" 
industry = "IT" 
instrument = "equity"
insertCompanyDataInTable(cursor, securityCode, securityLink, securityName, compName, secStatus, faceVal, industry, instrument)
securityCode = 1299
securityLink = "http://google.com"
securityName = "goog" 
compName = "google" 
secStatus = "active" 
faceVal = "23" 
industry = "IT" 
instrument = "equity"
insertCompanyDataInTable(cursor, securityCode, securityLink, securityName, compName, secStatus, faceVal, industry, instrument)
printCompanyDataFromTable(cursor)
# disconnect from server
db.close()

#features 
#----------------------------------------
#   connect to DB
#   check if table exists else create a table
#   check if data exists in table
#   insert data in table 
#   insert marking a field as unique 
#   query data in table 
#   iterate over all the data in the table
