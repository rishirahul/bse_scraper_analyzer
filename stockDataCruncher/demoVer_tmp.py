import sys
from selenium import webdriver
import time
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import random
from random import shuffle
import csv
import MySQLdb
import os
import time
from time import mktime
from datetime import datetime

def insertCompanyDataInTable(cursor, secDate, secDateUni, secCode, opprice, clprice, hiprice, loprice, avgrprice, numshares, numTrades, delivQty):
    a = "('{0}', '{1}', '{2}', '{3}', '{4}' , '{5}', '{6}', '{7}', '{8}', '{9}' )".format(secDateUni, secCode, opprice, clprice, hiprice, loprice, avgrprice, numshares, numTrades, delivQty)
    print (a);
    sql = """INSERT DELAYED INTO CCD (sdateuni, scode, oprice, cprice, hprice, lprice, avgprice, nshares, ntrades, delqty) VALUES ({0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}' )""".format((secDateUni), str(secCode), str(opprice), str(clprice), str(hiprice), str(loprice), str(avgrprice),  \
        str(numshares), str(numTrades), str(delivQty))
    # cursor.execute(sql)
    try:
        cursor.execute(sql)
    except Exception as e:
        print ("The following query failed:")
        print (sql) 
        print (e)
        sqll = """SELECT * FROM CCD WHERE sdateuni > 0"""
        try:
            # Execute the SQL command
            cursor.execute(sqll)
            results = cursor.fetchall()
            print (results)
        except Exception as ee: 
            print ("can search entry")
            print (ee)

def checkTableExists(dbcon, tablename):
    dbcur = dbcon.cursor()
    dbcur.execute("""
        SELECT COUNT(*)
        FROM information_schema.tables
        WHERE table_name = '{0}'
        """.format(tablename.replace('\'', '\'\'')))
    if dbcur.fetchone()[0] == 1:
        dbcur.close()
        return True
    dbcur.close()
    return False

def fetchAndStoreCompanyData(scurityCode, cursor):
    securityCode = 500002
    # url = 'https://www.bseindia.com/markets/equity/EQReports/StockPrcHistori.aspx?scripcode=%d&flag=sp&Submit=G' % securityCode
    # print(url)
    downloadedFile = "/home/karma/Downloads/%d.csv" % securityCode
    print(downloadedFile)
    # driver = webdriver.Chrome()
    # driver.get(url)

    # driver.find_element_by_xpath('//img[@id="imgCal"]').click
    # driver.find_element_by_xpath('//input[@id="ctl00_ContentPlaceHolder1_txtFromDate"]').click()
    # driver.find_element_by_xpath('//input[@id="ctl00_ContentPlaceHolder1_txtFromDate"]').clear()
    # driver.find_element_by_xpath('//input[@id="ctl00_ContentPlaceHolder1_txtFromDate"]').send_keys("01/01/2010")
    # time.sleep(2)
    # driver.find_element_by_xpath("//select[@name='ddlCalMonthctl00_ContentPlaceHolder1_divMFdate']/option[text()='Feb']").click()
    # time.sleep(2)
    # driver.find_element_by_xpath("//select[@name='ddlCalYearctl00_ContentPlaceHolder1_DivDate1']/option[text()='2010']").click()
    # driver.find_element_by_xpath("//td[@class='weekday']/a[text()='1']").click()
    # time.sleep(2)
    # driver.find_element_by_xpath('//img[@id="imgCal1"]').click
    # time.sleep(2)
    # time.sleep(10)
    # driver.find_element_by_xpath('//input[@id="ctl00_ContentPlaceHolder1_txtToDate"]').click()
    # time.sleep(2)
    # driver.find_element_by_xpath("//input[@id='ctl00_ContentPlaceHolder1_btnSubmit']").click()
    # time.sleep(5)
    # driver.find_element_by_xpath("//input[@id='ctl00_ContentPlaceHolder1_btnDownload1']").click()
    # time.sleep(20)


    print ("download complete")
    #store company data
    fields = []
    rows = []
    with open(downloadedFile, 'r') as csvfile:
        print("opened for read")
        csvreader = csv.reader(csvfile)
        fields = next(csvreader)
        for row in csvreader:
            rows.append(row)
        print("Total no. of rows: %d"%(csvreader.line_num))
    print('Field names are:' + ', '.join(field for field in fields))
    print('\nFirst 5 rows are:\n')
    for row in rows[:5]:
        struct_time = time.strptime(row[0], "%d-%B-%Y")
        secDate = struct_time 
        # dt = datetime.fromtimestamp(mktime(secDate)
        # print dt
        # print (type(dt))
        # print dt.day
        # print dt.month
        # print dt.year
        secCode = securityCode 
        opprice = float(row[1])
        hiprice = float(row[2])
        loprice = float(row[3])
        clprice = float(row[4])
        avgrprice =  float(row[5])
        numshares =  int(float(row[6]))
        numTrades =  int(float(row[7]))
        delivQty = int(float(row[9]))
        secDateUni = (struct_time.tm_year*10000)+(struct_time.tm_mon*100)+(struct_time.tm_mday)
        insertCompanyDataInTable(cursor, secDate, secDateUni, secCode, opprice, clprice, hiprice, loprice, avgrprice, numshares, numTrades, delivQty)
        print(struct_time.tm_mday)
        print(struct_time.tm_year)
        print(struct_time.tm_mon)
        print((struct_time.tm_year*10000)+(struct_time.tm_mon*100)+(struct_time.tm_mday))
        #print(type(struct_time))
        for col in row:
            print("%10s"%col),
        print('\n')

    print ("read complete")
    #delete downloaded file from disk
    # os.system('rm -rf %s' % downloadedFile)

    # driver.close()
    # driver.quit()

def processCompanyDataFromTable(cursor):
    sql = """SELECT * FROM COMPANY WHERE scode > 0"""
    i = 1
    try:
        cursor.execute(sql)
        results = cursor.fetchall()
        for row in results:
            i = i + 1
            securityCode = row[0]
            companyCode = row[2]
            print (securityCode)
            print (companyCode)
            if (i < 4): 
                continue
            if (i > 6): 
                break
            fetchAndStoreCompanyData(securityCode, cursor)
    except:
        print ("Error: unable to fecth data")

def iterateOverCompaniesAndPopulateData():
    # Open database connection
    db = MySQLdb.connect("localhost","root","mayur_1")
    cursor = db.cursor()
    checkTableExists(db, "CCD")
    db.commit()
    # createDBifDNE(db, 'demoDb')
    sql = 'use demoDb'
    cursor.execute(sql)
    processCompanyDataFromTable(cursor)
    cursor.close()
    db.commit()
    db.close()

def main(argv):
    iterateOverCompaniesAndPopulateData();

if __name__ == "__main__":
    main(sys.argv)