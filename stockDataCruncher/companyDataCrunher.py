##### create db using ######
# CREATE TABLE COMPANY_DATA ( sdate DATE NOT NULL, scode INT NOT NULL, oprice FLOAT(16,2) NOT NULL, cprice FLOAT(16,2) NOT NULL, 
#             hprice FLOAT(16,2) NOT NULL, lprice FLOAT(16,2) NOT NULL, avgprice FLOAT(16,2) NOT NULL, nshares INT NOT NULL, ntrades INT NOT NULL, 
#             delqty INT NOT NULL, PRIMARY KEY ( scode, sdate )) PARTITION BY HASH(scode)
############################
import sys
from selenium import webdriver
import time
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
import random
from random import shuffle
import csv
import MySQLdb
import os
import time
from time import mktime
from datetime import datetime
import os.path
#from Sets import Set

def insertCompanyDataInTable(cursor, secDate, secDateUni, secCode, opprice, clprice, hiprice, loprice, avgrprice, numshares, numTrades, delivQty):
    # a = "('{0}', '{1}', '{2}', '{3}', '{4}' , '{5}', '{6}', '{7}', '{8}', '{9}' )".format(secDateUni, secCode, opprice, clprice, hiprice, loprice, avgrprice, numshares, numTrades, delivQty)
    # print (a);
    sql = """INSERT IGNORE INTO COMPANY_DATA (sdateuni, scode, oprice, cprice, hprice, lprice, avgprice, nshares, ntrades, delqty) VALUES ({0}, '{1}', '{2}', '{3}', '{4}', '{5}', '{6}', '{7}', '{8}', '{9}' )""".format((secDateUni), str(secCode), str(opprice), str(clprice), str(hiprice), str(loprice), str(avgrprice),  \
        str(numshares), str(numTrades), str(delivQty))
    try:
        cursor.execute(sql)
    except Exception as e:
        print ("The following query failed:")
        print (sql) 
        print (e)

def fetchAndStoreCompanyData(securityCode, cursor):
    url = 'https://www.bseindia.com/markets/equity/EQReports/StockPrcHistori.aspx?scripcode=%d&flag=sp&Submit=G' % securityCode
    print(url)
    downloadedFile = "/home/karma/Downloads/%d.csv" % securityCode
    downloadedFileV2 = "/home/karma/Downloads/%d.csv\n" % securityCode
    badCompList = "/home/karma/Downloads/bclist.txt" 
    print(downloadedFile)

    badCompanies = set(["a"])
    with open(badCompList, 'r+') as bcfile:
        lines = bcfile.readlines()
        for line in lines:
            badCompanies.add(line)
    #print(badCompanies)
    #time.sleep(6)
    
    if (os.path.exists(downloadedFile)):
        print("path already exists: %s", downloadedFile)
        return
    elif (downloadedFileV2 in badCompanies):
        print("path already exists: %s", downloadedFile)
        return
    driver = webdriver.Chrome()
    driver.get(url)
    print("Downloading file from: %s", url)
    # driver.find_element_by_xpath('//img[@id="imgCal"]').click
    try:
        time.sleep(3)
        driver.find_element_by_xpath('//input[@id="ctl00_ContentPlaceHolder1_txtFromDate"]').click()
        driver.find_element_by_xpath('//input[@id="ctl00_ContentPlaceHolder1_txtFromDate"]').clear()
        driver.find_element_by_xpath('//input[@id="ctl00_ContentPlaceHolder1_txtFromDate"]').send_keys("01/01/2010")
        time.sleep(1)
        driver.find_element_by_xpath('//input[@id="ctl00_ContentPlaceHolder1_txtToDate"]').click()
        time.sleep(1)
        driver.find_element_by_xpath("//input[@id='ctl00_ContentPlaceHolder1_btnSubmit']").click()
        time.sleep(4)
        driver.find_element_by_xpath("//input[@id='ctl00_ContentPlaceHolder1_btnDownload1']").click()
        time.sleep(6)
    except Exception as e:
        with open(badCompList, 'a+') as bcfile:
            bcfile.write(downloadedFile);
            bcfile.write("\n");
            badCompanies.add(downloadedFile)
        driver.close()
        print("Bad company data")
        print(e)
        return


    print ("download complete")
    #store company data
    fields = []
    rows = []
    with open(downloadedFile, 'r') as csvfile:
        print("opened for read")
        csvreader = csv.reader(csvfile)
        fields = next(csvreader)
        for row in csvreader:
            rows.append(row)
        print("Total no. of rows: %d"%(csvreader.line_num))
    print('Field names are:' + ', '.join(field for field in fields))
    print('\nFirst 5 rows are:\n')
    i = 0
    for row in rows:
        i = i + 1
        struct_time = time.strptime(row[0], "%d-%B-%Y")
        secDate = struct_time 
        secCode = securityCode 
        # print("inserting %s %s %s %s %s %s %s %s", row[1], row[2], row[3], row[4], row[5], row[6], row[7], row[9])
        opprice = (row[1])
        hiprice = (row[2])
        loprice = (row[3])
        clprice = (row[4])
        avgrprice =  (row[5])
        numshares =  ((row[6]))
        numTrades =  ((row[7]))
        delivQty = ((row[9]))
        secDateUni = (struct_time.tm_year*10000)+(struct_time.tm_mon*100)+(struct_time.tm_mday)
        if ((i%100) == 0):
            print (i);
            # time.sleep(2)
        insertCompanyDataInTable(cursor, secDate, secDateUni, secCode, opprice, clprice, hiprice, loprice, avgrprice, numshares, numTrades, delivQty)

    print ("read complete")
    #delete downloaded file from disk
    # os.system('rm -rf %s' % downloadedFile)

    driver.close()
    driver.quit()

def processCompanyDataFromTable(cursor):
    sql = """SELECT * FROM COMPANY WHERE scode > 0"""
    i = 1
    try:
        cursor.execute(sql)
        results = cursor.fetchall()
        for row in results:
            i = i + 1
            securityCode = row[0]
            companyCode = row[2]
            print (securityCode)
            print (companyCode)
            # if (i < 4): 
            #     continue
            # if (i > 6): 
            #     break
            fetchAndStoreCompanyData(securityCode, cursor)
    except  Exception as e:
        print ("The following query failed:")
        print (sql) 
        print (e)

def iterateOverCompaniesAndPopulateData():
    # Open database connection
    db = MySQLdb.connect("localhost","root","mayur_1")
    cursor = db.cursor()
    # createDBifDNE(db, 'demoDb')
    sql = 'use IndianStockData'
    cursor.execute(sql)
    processCompanyDataFromTable(cursor)

def main(argv):
    iterateOverCompaniesAndPopulateData();

if __name__ == "__main__":
    main(sys.argv)
