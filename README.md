# BSE_Scraper_analyzer

This repository contains python code to:
- scrap list of all stocks listed in BSE
- crawl and get value of all stocks in BSE in last 3 years
- Store stocks in SQL
- analyze price of stocks 
